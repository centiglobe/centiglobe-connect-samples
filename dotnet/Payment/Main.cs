﻿using JsonObject;

namespace Main
{
    public class MainClass
    {

        public static void Main(string[] args)
        {
            string method = getArg(args, 0);
            if (method.Equals("payment"))
            {
                ParseAndRunFull(args);
            }
            else if (method.Equals("listBasketFcats"))
            {
                ParseAndRunListBasketFcats(args);
            }
            else
            {
                DisplayHelp();
            }
        }
        private static void DisplayHelp()
        {
            Console.WriteLine("Centiglobe Payment example:");
            Console.WriteLine();
            Console.WriteLine("  Perform a full payment");
            Console.WriteLine("  Example:");
            Console.WriteLine("    payment -account member1 -key \"privateJWK\" -url https://staging.connect.centiglobe.com:8000/");
            Console.WriteLine("      -cur SEK -userpool eu-west-1_Osj1NrDI6 -clientid id -username alice -password \"thisIsAPassword\"");
            Console.WriteLine("  List basket FCATs");
            Console.WriteLine("  Example:");
            Console.WriteLine("    listBasketFcats -userpool <userpool> -clientid <clientid> -username <username> -password <password>");
            Console.WriteLine("      -url https://staging.connect.centiglobe.com:8000/" );
        }

        
        private static void ParseAndRunFull(string[] args)
        {
            string? account = null;
            string? privateJWKAsString = null;
            string? baseURLAsString = null;
            string? currency = null;
            string? userpoolId = null;
            string? clientId = null;
            string? username = null;
            string? password = null;
            for (int i = 1; i < args.Length; i++)
            {
                if (getArg(args, i).Equals("-account"))
                {
                    account = getArg(args, ++i);
                }
                else if (getArg(args, i).Equals("-cur"))
                {
                    currency = getArg(args, ++i);
                }
                else if (getArg(args, i).Equals("-key"))
                {
                    privateJWKAsString = getArg(args, ++i);
                }
                else if (getArg(args, i).Equals("-url"))
                {
                    baseURLAsString = getArg(args, ++i);
                }
                else if (getArg(args, i).Equals("-userpool"))
                {
                    userpoolId = getArg(args, ++i);
                }
                else if (getArg(args, i).Equals("-clientid"))
                {
                    clientId = getArg(args, ++i);
                }
                else if (getArg(args, i).Equals("-username"))
                {
                    username = getArg(args, ++i);
                }
                else if (getArg(args, i).Equals("-password"))
                {
                    password = getArg(args, ++i);
                }
            }
            if (account == null ||
                currency == null ||
                privateJWKAsString == null ||
                baseURLAsString == null ||
                clientId == null ||
                userpoolId == null ||
                username == null ||
                password == null
                )
            {
                DisplayHelp();
                return;
            }

            Payment.AccessTokenData accessTokenData = new Payment.AccessTokenData(userpoolId, clientId, username, password);
            PaymentInstruction paymentInstruction = GetPaymentInstruction(account, currency);
            Program.Payment(paymentInstruction, accessTokenData, privateJWKAsString!, baseURLAsString!).GetAwaiter().GetResult();
        }

        private static PaymentInstruction GetPaymentInstruction(string payinMemberName, string payinCurrency)
        {
            PaymentInstruction paymentInstruction = new PaymentInstruction();
            paymentInstruction.payoutMemberName = "payout-fake";
            paymentInstruction.payoutCurrency = "SEK";
            paymentInstruction.payoutService = "INTEGRATION";
            paymentInstruction.amlInformation = GetAmlInformation();
            paymentInstruction.payinMemberName = payinMemberName;
            paymentInstruction.payinCurrency = payinCurrency;
            paymentInstruction.fixedAmountSide = "PAYOUT";
            paymentInstruction.fixedAmount = "1";
            return paymentInstruction;
        }

        
        
        private static Dictionary<String, Object> GetAmlInformation()
        {
            var amlInformation = new Dictionary<string, object>();

            amlInformation["$schema"] = "https://schema.connect.centiglobe.com/aml/20240502-cg-integration-4319ce0ad64899c4.json";

        
            var sender = new Dictionary<string, object>
            {
                ["name"] = new Dictionary<string, string>
                {
                    ["firstName"] = "Alice",
                    ["lastName"] = "Andersson",
                    ["entityType"] = "Person"
                },
                ["addressField"] = "Klemkestraße 1240",
                ["postalCode"] = "13409",
                ["city"] = "Berlin",
                ["country"] = "DE"
            };
            amlInformation["sender"] = sender;

        
            var recipient = new Dictionary<string, object>
            {
                ["name"] = new Dictionary<string, string>
                {
                    ["firstName"] = "Bob",
                    ["lastName"] = "Andersson",
                    ["entityType"] = "Person"
                },
                ["addressField"] = "Reimersholmsgatan 104",
                ["postalCode"] = "11740",
                ["city"] = "Stockholm",
                ["country"] = "SE"
            };
            amlInformation["recipient"] = recipient;

            var targetAccount = new Dictionary<string, object>
            {
                ["accountIdentifier"] = new Dictionary<string, string>
                {
                    ["ibanNumber"] = "SE5675512358273223854822",
                    ["identifierType"] = "IBAN"
                },
                ["country"] = "SE"
            };
            amlInformation["targetAccount"] = targetAccount;

            amlInformation["purposeOfTransaction"] = "GIFT";
            amlInformation["reference"] = "b6c40f51-94d0-4bd3-bd92-a4da185b3a5c";

            return amlInformation;
        }
        
         public static void ParseAndRunListBasketFcats(string[] args)
        {
            string? baseURLAsString = null;
            string? userpoolId = null;
            string? clientId = null;
            string? username = null;
            string? password = null;
            for (int i = 1; i < args.Length; i++)
            {
                if (getArg(args, i).Equals("-url"))
                {
                    baseURLAsString = getArg(args, ++i);
                }
                else if (getArg(args, i).Equals("-userpool"))
                {
                    userpoolId = getArg(args, ++i);
                }
                else if (getArg(args, i).Equals("-clientid"))
                {
                    clientId = getArg(args, ++i);
                }
                else if (getArg(args, i).Equals("-username"))
                {
                    username = getArg(args, ++i);
                }
                else if (getArg(args, i).Equals("-password"))
                {
                    password = getArg(args, ++i);
                }
            }
            if (baseURLAsString == null ||
                clientId == null ||
                userpoolId == null ||
                username == null ||
                password == null)
            {
                DisplayHelp();
                return;
            }
            Payment.AccessTokenData accessTokenData = new Payment.AccessTokenData(userpoolId, clientId, username, password);
            ListBasketFcatsResponse listBasketFcatsResponse = Program.ListBasketFcats(
                accessTokenData,
                baseURLAsString).GetAwaiter().GetResult();
            Console.WriteLine("Basket FCATs: ");
            Console.WriteLine("------------------------");
            listBasketFcatsResponse.results!.ToList().ForEach(i =>
            {
                Console.WriteLine("memberName: " + i.memberName);
                Console.WriteLine("basketFcatName: "+ i.basketFcatName);
                Console.WriteLine("balance: " + i.balance);
                Console.WriteLine("------------------------");
            });
        }
         
        public static string getArg(string[] args, int number)
        {
            if (args.Length <= number)
            {
                DisplayHelp();
                Environment.Exit(1);
            }
            return args[number];
        }

    }
}
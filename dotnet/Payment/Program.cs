using System.Text.Json;
using JsonObject;
using Payment;

public class Program
{
    public static async Task Payment(PaymentInstruction paymentInstruction, 
                                    AccessTokenData accessTokenData,
                                    string privateJWKAsString,
                                    string baseURLAsString)
    {
        JsonObject.PrivateJWK privateJwk = JsonSerializer.Deserialize<PrivateJWK>(privateJWKAsString)!;
        var privateJwkSigningCredentials = SecurityUtil.CreateSignCredentialFromPrivateKey(privateJwk);
        CognitoAccessTokenProvider accessTokenProvider = await CognitoAccessTokenProvider.Create(accessTokenData);
        using (PaymentAPI api = new PaymentAPI(new Uri(baseURLAsString)))
        {
            // Generate accessToken
            string accessToken = await accessTokenProvider.GetAccessToken();

            /** Prepare payment */

            // Make a request to the prepare payment endpoint to prepare a payment
            PreparePaymentResponse preparePaymentResponse = await api.PreparePayment(paymentInstruction, accessToken);
            
            /** Verifying the response from prepare payment request */

            // Fetch the payout's public JWK
            PublicJWK[] payoutPublicJWK = await api.GetPayoutJWK(paymentInstruction.payoutMemberName, accessToken);

            // Verify that the payment statement has been signed by the payout's JWK
            PaymentStatement verifiedPaymentStatement =
                SecurityUtil.VerifyJWS<PaymentStatement>(preparePaymentResponse.signedPaymentStatement!,
                    payoutPublicJWK);

            Console.WriteLine("Successfully verified signature for the payment statement");
            Console.WriteLine("  valid until: " + verifiedPaymentStatement.validUntil);
            Console.WriteLine("  payin member name:: " + verifiedPaymentStatement.payin!.memberName);
            Console.WriteLine("  payout member name: " + verifiedPaymentStatement.payout!.memberName);
            Console.WriteLine("  currency:  " + verifiedPaymentStatement.payout.currency);
            Console.WriteLine("  fee: " + verifiedPaymentStatement.payout.fee);
            Console.WriteLine("  amountMax: " + verifiedPaymentStatement.payout.amountMax);
            // Here we can validate that we want to make the payment

            /* Create Payment */
          
            // The external payment reference to find this payment 
            string paymentRef = System.Guid.NewGuid().ToString();

            string paymentStatementHash = preparePaymentResponse.paymentStatementHash!;
            string signedPaymentStatementHash =
                SecurityUtil.CreateJWS(paymentStatementHash, privateJwkSigningCredentials);
            
            PaymentOrder paymentOrder = new PaymentOrder();
            paymentOrder.paymentRef = paymentRef;
            paymentOrder.paymentStatementHash = paymentStatementHash;
            paymentOrder.signedPaymentStatementHash = signedPaymentStatementHash;
            paymentOrder.payinCurrency = paymentInstruction.payinCurrency;
            paymentOrder.triggerType = "IMMEDIATE";
            paymentOrder.fixedAmountSide = paymentInstruction.fixedAmountSide;
            paymentOrder.fixedAmount = paymentInstruction.fixedAmount;
            
            /*********************************************************************************************
             * WorstAcceptablePrice is a safeguard amount to ensure a payment                            *
             * only gets accepted if the actual payin amount <= worstAcceptablePrice                     *
             * In short, it's how much you are willing to pay at most.                                   *
             *                                                                                           *
             * The indicative amount we got from the prepare payment response                            *
             * represents what we are expected to pay.                                                   *
             * The final payment amount fluctuate based on the exchange rate, which means that the final *
             * payment amount won't necessarily be the same as the indicative amount.                    *
             *********************************************************************************************/

            // As an example we will set the worstAcceptable price to 1% more than the indicative amount
            string worstAcceptablePrice = Decimal
                .Multiply(Convert.ToDecimal(preparePaymentResponse.payinAmount), new Decimal(1.01d)).ToString();

            PaymentStatusResponse paymentResponse =
                await api.CreatePayment(paymentOrder, worstAcceptablePrice, accessToken);

            /** Verifying the response from payment request */

            // We expect the payment to go through without any problems
            // If this is the case the state of the payment should be ACCEPTED
            if (paymentResponse.payment!.GetPaymentState() == PaymentState.ACCEPTED)
            {
                Console.WriteLine(
                    "Payment was received by payout with the payoutId " + paymentResponse.payment.payoutId);
            }
        }
    }
 
    public static async Task<ListBasketFcatsResponse> ListBasketFcats(AccessTokenData accessTokenData,
                                                                      string baseURLAsString)
    {
        CognitoAccessTokenProvider accessTokenProvider = await CognitoAccessTokenProvider.Create(accessTokenData);
        using (PaymentAPI api = new PaymentAPI(new Uri(baseURLAsString)))
        {
            // Generate accessToken
            string accessToken = await accessTokenProvider.GetAccessToken();
            // Make a request to the list basket FCATs endpoint to list the basket FCATs
            return await api.ListBasketFcats(accessToken);
        }
    }
}
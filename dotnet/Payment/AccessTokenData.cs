namespace Payment;

public class AccessTokenData
{
    public AccessTokenData(string userPoolId, string clientId, string username, string password)
    {
        this.userPoolId = userPoolId;
        this.clientId = clientId;
        this.username = username;
        this.password = password;
    }
    public string userPoolId { get;}
    public string clientId { get; }
    public string username { get; }
    public string password { get; }
    
    public string GetRegionSystemName()
    {
        return userPoolId.Split("_")[0];
    }

}
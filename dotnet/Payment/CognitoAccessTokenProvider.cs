namespace Payment;

using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.Extensions.CognitoAuthentication;
public class CognitoAccessTokenProvider
{
    private readonly CognitoUser _user;
    private CognitoAccessTokenProvider(CognitoUser user)
    {
        _user = user;
    }

    public async Task<string> GetAccessToken()
    {
        if(ShouldRefreshSession()){
            await RefreshSession();
        }
        return _user.SessionTokens.AccessToken;
    }

    /**
     * In the example code the session is refreshed for each request
     * even though it is not needed.
     *
     * For production code the session can be refreshed before the Access Token
     * expires.
     */
    private static bool ShouldRefreshSession()
    {
        return true;
    }

    private async Task RefreshSession()
    {
        InitiateRefreshTokenAuthRequest authRequest = new InitiateRefreshTokenAuthRequest()
        {
            AuthFlowType = AuthFlowType.REFRESH_TOKEN_AUTH
        };
        // Updates current session of the user 
        await _user.StartWithRefreshTokenAuthAsync(authRequest).ConfigureAwait(false);
    }

    public static async Task<CognitoAccessTokenProvider> Create(AccessTokenData accessTokenData)
    {
        String region = accessTokenData.GetRegionSystemName();
        AmazonCognitoIdentityProviderClient provider =
            new AmazonCognitoIdentityProviderClient(new Amazon.Runtime.AnonymousAWSCredentials(), RegionEndpoint.GetBySystemName(region));
        CognitoUserPool userPool = new CognitoUserPool(accessTokenData.userPoolId, accessTokenData.clientId, provider);
        CognitoUser user = new CognitoUser(accessTokenData.username, accessTokenData.clientId, userPool, provider);
        InitiateSrpAuthRequest authRequest = new InitiateSrpAuthRequest()
        {
            Password = accessTokenData.password
        };
        // Updates current session of the user 
       await user.StartWithSrpAuthAsync(authRequest).ConfigureAwait(false);
       return new CognitoAccessTokenProvider(user);
    }
}

using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;
using System.Text.Json;

public class SecurityUtil
{
    public static string CreateJWS(string payloadToSign, SigningCredentials signingCredentials){
        string headers = Base64UrlEncoder.Encode("{\"alg\": \"ES256\"," +
                                                 "\"kid\": \"" + signingCredentials.Kid + "\"}");
        string payload = Base64UrlEncoder.Encode(payloadToSign);
        string signdata = Microsoft.IdentityModel.JsonWebTokens.JwtTokenUtilities.CreateEncodedSignature(headers + "." + payload, signingCredentials);
        return headers + "." + payload + "." + signdata;
    }
    
    public static T VerifyJWS<T>(string unverifiedJWS, JsonObject.PublicJWK[] payoutPublicJWK)
    {
        var publicSecurityKey = payoutPublicJWK.Select(CreatePublicKeyFromJWK);
        SecurityToken securityToken;
        // This is a work around for no being able to find a suitable library for doing the JWS verification
        // of the payload. Here we are verifying that the signature is correct for a JwtSecurityToken.
        // The verification uses the same verification schema but has a different underlaying data in the
        // payload field.
        var jwtHandler = new JwtSecurityTokenHandler();
        jwtHandler.ValidateToken(unverifiedJWS,
        new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKeys = publicSecurityKey,
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateLifetime = false,
        }, out securityToken);
        return JwtSecurityTokenToJsonObject<T>((securityToken as JwtSecurityToken)!);
    }

    private static T JwtSecurityTokenToJsonObject<T>(JwtSecurityToken token)
    {
        JwtPayload jwtPayload = token.Payload;
        string intermediateString = jwtPayload.SerializeToJson();
        return JsonSerializer.Deserialize<T>(intermediateString)!;
    }

    public static SigningCredentials CreateSignCredentialFromPrivateKey(JsonObject.PrivateJWK privateJwk)
    {
        ECDsa key = ECDsa.Create(new ECParameters
        {
            Curve = ECCurve.NamedCurves.nistP256,
            D = Base64UrlEncoder.DecodeBytes(privateJwk.d),
            Q = new ECPoint
            {
                X = Base64UrlEncoder.DecodeBytes(privateJwk.x),
                Y = Base64UrlEncoder.DecodeBytes(privateJwk.y)
            }
        });

        ECDsaSecurityKey securityKey =  new ECDsaSecurityKey(key);
        securityKey.KeyId = privateJwk.kid;
        return new SigningCredentials(securityKey, "ES256");
    }
    
    private static SecurityKey CreatePublicKeyFromJWK(JsonObject.PublicJWK publicJwk)
    {
        ECDsa key = ECDsa.Create(new ECParameters
        {
            Curve = ECCurve.NamedCurves.nistP256,
            Q = new ECPoint
            {
                X = Base64UrlEncoder.DecodeBytes(publicJwk.x),
                Y = Base64UrlEncoder.DecodeBytes(publicJwk.y)
            }
        });
        ECDsaSecurityKey securityKey = new ECDsaSecurityKey(key);
        securityKey.KeyId = publicJwk.kid;
        return securityKey;
    }

}
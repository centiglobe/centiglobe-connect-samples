namespace JsonObject
{
    public class PaymentInstruction
    {
        public string? payinMemberName { get; set; }
        
        public string? payinCurrency { get; set; }

        public string? payoutMemberName { get; set; }
        
        public string? payoutCurrency { get; set; }

        public string? payoutService { get; set; }
        
        public string? fixedAmountSide { get; set; }
        
        public string? fixedAmount { get; set; }
        public Dictionary<string, Object>? amlInformation { get; set; }
    }

    
    public class PaymentStatusResponse {
        public PaymentData? payment { get; set; }
        public PayinDetailsData? details  { get; set; }
    }
    
    public class PreparePaymentResponse
    {
        public string? paymentStatementHash { get; set; }
        public string? fixedAmountSide { get; set; }
        public string? payinCurrency { get; set; }
        public string? payinAmount { get; set; }
        public string? payoutAmount { get; set; }
        public string? signedPaymentStatement { get; set; }
    }

    public class PayinAmount
    {
        public string? currency { get; set; }
        public string? indicativeAmount { get; set; }
    }

    public class PrivateJWK
    {
        public string? kid { get; set; }
        public string? alg { get; set; }
        public string? use { get; set; }
        public string? kty { get; set; }
        public string? x { get; set; }
        public string? y { get; set; }
        public string? crv { get; set; }
        public string? d { get; set; }
    }

    public class PublicJWK
    {
        public string? kid { get; set; }
        public string? alg { get; set; }
        public string? use { get; set; }
        public string? kty { get; set; }
        public string? x { get; set; }
        public string? y { get; set; }
        public string? crv { get; set; }
    }

    public class WellKnowJwks
    {
        public PublicJWK[]? keys { get; set; }
    }

    public class PaymentStatement
    {
        public string? createdAt { get; set; }
        public string? validUntil { get; set; }
        public Payin? payin { get; set; }
        public Payout? payout { get; set; }
        public string? paymentType { get; set; }
        public Dictionary<string, Object>? amlInformation { get; set; }
    }

    public class Payin
    {
        public string? memberName { get; set; }
    }

    public class Payout
    {
        public string? memberName { get; set; }
        public string? currency { get; set; }
        public string? fee { get; set; }
        public string? service { get; set; }
        public string? assetName { get; set; }
        public string? amountMin { get; set; }
        public string? amountMax { get; set; }
    }

    public class PaymentOrder
    {
        public string? signedPaymentStatementHash { get; set; }
        public string? paymentStatementHash { get; set; }
        public string? payinCurrency { get; set; }
        public string? fixedAmountSide { get; set; }
        public string? fixedAmount { get; set; }
        public string? triggerType { get; set; }
        public string? paymentRef { get; set; }
    }
    
    public class PayinDetailsData
    {
        public ValueTransferAmount? valueTransferAmount { get; set; }
        public PayinSpecifics? payinSpecifics { get; set; }
        public PaymentAmount? payoutAmount { get; set; }
    }

    public class PayinSpecifics
    {
        public PaymentAmount? payinAmount { get; set; }
        public string? paymentRef { get; set; }
        public string? recreatedFrom { get; set; }
        public string? recreatedTo { get; set; }
    }

    public class ValueTransferAmount
    {
        public string? valueTransferToken { get; set; }
        public string? amount { get; set; }
    }

    public class PaymentData
    {
        public string? paymentId { get; set; }
        public string? paymentStatementHash { get; set; }
        public string? createdAt { get; set; }
        public string? payinMemberName { get; set; }
        public string? payoutMemberName { get; set; }
        public PaymentAmount? paymentAmount { get; set; }
        public string? state { get; set; }
        
        public string? paymentType { get; set; }

        public string? payoutId { get; set; }

        public PaymentState GetPaymentState()
        {
            if (Enum.TryParse<PaymentState>(state, true, out var result))
            {
                return result;
            }
            throw new Exception("Failed to parse enum");
        }
    }

    public enum PaymentState
    {
        FUNDED,
        AWAITING_ACCEPT,
        ACCEPTED,
        REFUNDED,
        TERMINATED
    }

    public class PaymentAmount
    {
        public string? assetName { get; set; }

        public string? amount { get; set; }
    }
    
    public class ListBasketFcatsResponse
    {
        public BasketFcatsResponse[]? results { get; set; }
        public int? count { get; set; }
        public string? next { get; set; }
    }
    
    public class BasketFcatsResponse
    {
        public string? memberName { get; set; }
        public string? basketFcatName { get; set; }
        public string? balance { get; set; }
    }
}
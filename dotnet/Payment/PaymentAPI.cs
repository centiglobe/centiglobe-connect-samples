using System.Text.Json;
public class PaymentAPI : IDisposable
{
    private HttpClient httpClient;
    public PaymentAPI(Uri baseUrl)
    {
        HttpClient tmpHttpClient = new HttpClient();
        tmpHttpClient.BaseAddress = baseUrl;
        tmpHttpClient.DefaultRequestHeaders.Accept.Clear();
        tmpHttpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        httpClient = tmpHttpClient;
    }

    public async Task<JsonObject.PreparePaymentResponse> PreparePayment(JsonObject.PaymentInstruction paymentInstruction, string accessToken)
    {
        HttpRequestMessage request = Post<JsonObject.PaymentInstruction>("payment-service/v2/payment-statements", accessToken, paymentInstruction);
        HttpResponseMessage responseMessage = await httpClient.SendAsync(request);
        return await ReadResultAsJson<JsonObject.PreparePaymentResponse>(responseMessage, "Failed to prepeare payment");
    }

    public async Task<JsonObject.PaymentStatusResponse> CreatePayment(JsonObject.PaymentOrder signedPaymentOrder, String payinLimit, string accessToken)
    {
        string uri = "payment-service/v2/payments?limitAmount=" + payinLimit;
        HttpRequestMessage request = Post<JsonObject.PaymentOrder>(uri, accessToken, signedPaymentOrder);
        HttpResponseMessage responseMessage = await httpClient.SendAsync(request);
        return await ReadResultAsJson<JsonObject.PaymentStatusResponse>(responseMessage, "Failed to create payment");
    }

    public async Task<JsonObject.PublicJWK[]> GetPayoutJWK(string? account, string? offChainBearer)
    {
        string uri = ".well-known/jwks/payout/" + account!;
        HttpRequestMessage request = Get(uri, offChainBearer!);
        HttpResponseMessage responseMessage = await httpClient.SendAsync(request);
        JsonObject.WellKnowJwks jwks = await ReadResultAsJson<JsonObject.WellKnowJwks>(responseMessage, "Failed to fetch public JWK");
        return jwks.keys!;
    }
    
    public async Task<JsonObject.ListBasketFcatsResponse> ListBasketFcats(string offChainBearer)
    {
        string uri = "token-service/basket-fcats";
        HttpRequestMessage request = Get(uri, offChainBearer);
        HttpResponseMessage responseMessage = await httpClient.SendAsync(request);
        return await ReadResultAsJson<JsonObject.ListBasketFcatsResponse>(responseMessage, "Failed to list basket FCATs");
    }

    private async Task<T> ReadResultAsJson<T>(HttpResponseMessage responseMessage, String error)
    {
        if (responseMessage.IsSuccessStatusCode)
        {
            string jsonString = await responseMessage.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<T>(jsonString)!;
        }
        else
        {
            throw new SystemException(error);
        }
    }

    private HttpRequestMessage Post<T>(String uri, string accessToken, T jsonToSend)
    {
        return create<T>(HttpMethod.Post, uri, accessToken, jsonToSend);
    }

    private HttpRequestMessage Get(String uri, string accessToken)
    {
        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, uri);
        request.Headers.Add("Authorization", "Bearer " + accessToken);
        return request;
    }

    private HttpRequestMessage create<T>(HttpMethod httpMethod, String uri, string accessToken, T jsonToSend)
    {
        HttpRequestMessage request = new HttpRequestMessage(httpMethod, uri);
        request.Content = new StringContent(JsonSerializer.Serialize(jsonToSend),
                                    System.Text.Encoding.UTF8,
                                    "application/json");
        string bearer = "Bearer " + accessToken;
        request.Headers.Add("Authorization", bearer);
        return request;
    }

    public void Dispose()
    {
        httpClient.Dispose();
    }
}

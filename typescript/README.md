# Examples

- `paymentExample.ts`: shows how to interact with Centiglobe's api to create a payment.
- `listBasketFcatsExample.ts`: shows how to interact with Centiglobe's api to list basket FCATs.

## How to run the payment example

1. Install npm packages

```shell
npm install
```

2. Go to `examples/paymentExample.ts`, add your private JWK, add login credentials and modify the payment instruction to
   a valid one.
3. Run the example:

```shell
npm run paymentExample
```

## How to run the list basket FCATs example

1. Install npm packages

```shell
npm install
```

2. Go to `examples/listBasketFcatsExample.ts` and add your login credentials.
3. Run the example:

```shell
npm run listBasketFcatsExample
```
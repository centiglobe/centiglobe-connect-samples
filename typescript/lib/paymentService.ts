import {
    BasketFcatAccountBalanceResponse,
    createBearerHeader,
    createContentTypeHeader, PaymentInstruction, PaymentOrder,
    PaymentStatusResponse,
    PreparePaymentResponse,
} from './paymentServiceUtils';
import {JWK} from 'jose';
import {v4 as uuidv4} from 'uuid';
import fetch, {RequestInfo, RequestInit} from 'node-fetch';

export async function preparePayment(
    paymentInstruction: PaymentInstruction,
    offChainBearer: string,
    baseUrl: string,
): Promise<PreparePaymentResponse> {
    const url = `${baseUrl}/payment-service/v2/payment-statements`;

    return await call<PreparePaymentResponse>(url, {
        headers: {...createBearerHeader(offChainBearer), ...createContentTypeHeader()},
        method: 'POST',
        body: JSON.stringify({...paymentInstruction}),
    });
}

export async function createPayment(
    paymentOrder: PaymentOrder,
    payinLimitAmount: string,
    offChainBearer: string,
    baseUrl: string,
): Promise<PaymentStatusResponse> {
    const url = `${baseUrl}/payment-service/v2/payments?limitAmount=${payinLimitAmount}`;

    return await call<PaymentStatusResponse>(url, {
        headers: {...createBearerHeader(offChainBearer), ...createContentTypeHeader()},
        method: 'POST',
        body: JSON.stringify({...paymentOrder}),
    });
}

export async function listBasketFcats(
    offChainBearer: string,
    baseUrl: string,
): Promise<BasketFcatAccountBalanceResponse> {
    const url = `${baseUrl}/token-service/basket-fcats`;

    return await call<BasketFcatAccountBalanceResponse>(url, {
        headers: {...createBearerHeader(offChainBearer)},
        method: 'GET',
    });
}

// Mock function for getting the payout JWK
export async function getPayoutJWK(
    payoutAccountName: string,
    offChainBearer: string,
    baseUrl: string,
): Promise<JWK[]> {
    const url = `${baseUrl}/.well-known/jwks/payout/${payoutAccountName}`;

    const response = await call<{ keys: JWK[] }>(url, {
        headers: {...createBearerHeader(offChainBearer)},
        method: 'GET',
    });

    return response.keys;
}

export async function call<T = Record<string, undefined>>(url: RequestInfo, init: RequestInit): Promise<T> {
    const uuid = uuidv4();

    init = {...{timeout: 25000}, ...init};

    console.log('Request to (correlation:' + uuid + '): ', url.toString());
    console.log('Request (correlation:' + uuid + '): ', init);

    const response = await fetch(url, init);
    const contentType = response.headers.get('content-type') || '';
    const jsonResponse = contentType.includes('application/json') ? await response.json() : {};

    console.log('Response (correlation:' + uuid + '): ', jsonResponse);

    if (!response.ok) {
        const error = {
            status: response.status,
            url,
            response: jsonResponse,
        };
        throw new Error(JSON.stringify(error));
    }

    return jsonResponse as Promise<T>;
}

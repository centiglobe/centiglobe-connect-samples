import {
    AuthenticationDetails,
    CognitoUser,
    CognitoUserPool,
    CognitoUserSession,
    ICognitoUserData
} from "amazon-cognito-identity-js";
import {AccessTokenData} from "./paymentServiceUtils";

export class CognitoBearerProvider {

    constructor(private readonly cognitoUser: CognitoUser, private currentSession: CognitoUserSession) {}

    public async getAccessTokenAsJWT(): Promise<string> {
        if(CognitoBearerProvider.shouldRefreshSession()){
            await this.refreshSession()
        }
        return this.currentSession.getAccessToken().getJwtToken()
    }

    /**
     * In the example code the session is refreshed for each request
     * even though it is not needed.
     *
     * For production code the session can be refreshed before the Access Token
     * expires.
     */
    private static shouldRefreshSession(): boolean {
        return true
    }

    private async refreshSession(): Promise<void> {
        const refreshToken = this.currentSession.getRefreshToken();
        const cognitoUserSession = new Promise<CognitoUserSession>((resolve, reject) => {
            this.cognitoUser.refreshSession(refreshToken, (err, newSession) => {
                if (err) reject(err);
                resolve(newSession);
            });
        });

        this.currentSession = await cognitoUserSession.catch(err => {
            console.log(err)
            throw new Error("Failed to update tokens")
        })
    }

    static async create(accessTokenData: AccessTokenData): Promise<CognitoBearerProvider>{
        const authenticationData = {
            Username: accessTokenData.username,
            Password: accessTokenData.password,
        };
        const authenticationDetails = new AuthenticationDetails(
            authenticationData
        );
        const poolData = {
            UserPoolId: accessTokenData.userPoolId,
            ClientId: accessTokenData.clientId,
        };
        const userPool = new CognitoUserPool(poolData);
        const userData: ICognitoUserData = {
            Username: accessTokenData.username,
            Pool: userPool,
        };
        const cognitoUser = new CognitoUser(userData);
        const session = await new Promise<CognitoUserSession>((resolve) => {
            cognitoUser.authenticateUser(authenticationDetails, {
                // eslint-disable-next-line  @typescript-eslint/no-explicit-any
                onFailure(err: any): void {
                    console.log(err);
                    throw new Error("Failed to login");
                },
                onSuccess: resolve
            })
        });
        return new CognitoBearerProvider(cognitoUser, session);
    }

}
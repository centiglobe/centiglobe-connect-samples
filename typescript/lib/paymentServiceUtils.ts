import { CompactSign, compactVerify, importJWK, JWK, KeyLike, createLocalJWKSet } from 'jose';
import { TextDecoder, TextEncoder } from 'util';

/* *
 * Types
 */

export type PaymentInstruction = {
  payinMemberName: string,
  payinCurrency: string,
  payoutMemberName: string,
  payoutCurrency: string,
  payoutService: string,
  fixedAmountSide: string,
  fixedAmount: string,
  amlInformation: Record<string, string>;
};

export type PreparePaymentResponse = {
  paymentStatementHash: string,
  fixedAmountSide: string,
  payinCurrency: string,
  payinAmount: string,
  payoutAmount: string,
  signedPaymentStatement: string;
};

export type PaymentAmount = {
  assetName: string;
  amount: string;
};

export type ValueTransferAmount = {
  valueTransferToken: string;
  amount: string;
};

export type PaymentState = 'FUNDED' | 'AWAITING_ACCEPT' | 'ACCEPTED' | 'REFUNDED' | 'TERMINATED';

export type PaymentStatusResponse = {
  payment: PaymentData,
  details: PayinDetailsData
}

export type PaymentData = {
  paymentId: string,
  paymentStatementHash: string,
  createdAt: string,
  payinMemberName: string,
  payoutMemberName: string,
  paymentAmount: PaymentAmount,
  state: PaymentState,
  paymentType: PaymentType,
  payoutId?: string
};

export type PayinDetailsData = {
  valueTransferAmount: ValueTransferAmount,
  payinSpecifics: PayinSpecifics,
  payoutAmount: PaymentAmount
}

export type PayinSpecifics = {
  payinAmount: PaymentAmount,
  paymentRef: string,
  recreatedFrom: string,
  recreatedTo: string
}

export type PaymentOrder = {
  signedPaymentStatementHash: string,
  paymentStatementHash: string,
  payinCurrency: string,
  fixedAmountSide: string,
  fixedAmount: string,
  triggerType: string,
  paymentRef: string
};

export type PaymentStatement = {
  createdAt: string,
  validUntil: string,
  payin: Payin,
  payout: Payout,
  paymentType: string,
  amlInformation: Record<string, string>;
}

export type Payin = {
  memberName: string
}

export type Payout = {
  memberName: string,
  currency: string,
  fee: string,
  service: string,
  assetName: string,
  amountMin: string,
  amountMax: string
}

export type AccessTokenData = {
  userPoolId: string;
  clientId: string;
  username: string;
  password: string;
}

export type BasketFcatAccountBalanceResponse = {
  memberName: string;
  basketFcatName: string;
  balance: string;
};

export type PaymentType = 'NORMAL' | 'REBALANCE';

/* *
 * Functions
 */

export async function createSignedPayload(payload: string, jwk: JWK) {
  const encoder = new TextEncoder();
  return new CompactSign(encoder.encode(JSON.stringify(payload)))
    .setProtectedHeader({
      alg: 'ES256',
      ...(jwk.kid && {kid: jwk.kid})
    })
    .sign(await jwkToKeyLike(jwk));
}

export async function jwkToKeyLike(jwk: JWK): Promise<KeyLike> {
  return importJWK(jwk) as Promise<KeyLike>;
}

export function createBearerHeader(token: string): { [key: string]: string } {
  return { Authorization: `Bearer ${token}` };
}

export function createContentTypeHeader(): { [key: string]: string } {
  return { 'Content-type': 'application/json' };
}

export async function verifyJWS(jws: string, jwk: JWK[]): Promise<PaymentStatement> {
  const jwksAsSet = {
    keys : jwk
  }
  const setOfJKWS = createLocalJWKSet(jwksAsSet);
  const { payload: verifiedPayload } = await compactVerify(jws, setOfJKWS);
  return JSON.parse(new TextDecoder().decode(verifiedPayload));
}

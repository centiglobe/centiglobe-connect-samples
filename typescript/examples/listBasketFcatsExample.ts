import {AccessTokenData} from "../lib/paymentServiceUtils";
import {CognitoBearerProvider} from "../lib/cognitoAccessTokenProvider";
import {listBasketFcats} from "../lib/paymentService";

// The login credentials to the login system
const accessTokenData: AccessTokenData = {
    userPoolId: 'ChangeMe',
    clientId: 'ChangeMe',
    username: 'ChangeMe',
    password: 'ChangeMe',
}

// The base url for the Centiglobe API
const baseUrl = 'https://staging.connect.centiglobe.com:8000';

listBasketFcatsExample(accessTokenData, baseUrl);

async function listBasketFcatsExample(accessTokenData: AccessTokenData, baseUrl: string) {
    const accessTokenAsJWT = await CognitoBearerProvider.create(accessTokenData).then(provider => provider.getAccessTokenAsJWT());
    await listBasketFcats(accessTokenAsJWT, baseUrl);
}
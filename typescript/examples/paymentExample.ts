import {
  AccessTokenData,
  createSignedPayload,
  PaymentInstruction,
  PaymentOrder,
  verifyJWS
} from '../lib/paymentServiceUtils';
import { createPayment, getPayoutJWK, preparePayment } from '../lib/paymentService';
import { JWK } from 'jose';
import { v4 as uuidv4 } from 'uuid';
import BigDecimal from 'js-big-decimal';
import {CognitoBearerProvider} from "../lib/cognitoAccessTokenProvider";

// The login credentials to the login system
const accessTokenData: AccessTokenData = {
  userPoolId: 'ChangeMe',
  clientId: 'ChangeMe',
  username: 'ChangeMe',
  password: 'ChangeMe',
}

// Your private JWK will be used for signing payloads
const privateJwk: JWK = {};

// The base url for the Centiglobe API
const baseUrl = 'https://staging.connect.centiglobe.com:8000';

const amlInformation: Record<string, any> = {
  $schema: "https://schema.connect.centiglobe.com/aml/20240502-cg-integration-4319ce0ad64899c4.json",
  sender: {
    name: {
      firstName: "Alice",
      lastName: "Andersson",
      entityType: "Person",
    },
    addressField: "Klemkestraße 1240",
    postalCode: "13409",
    city: "Berlin",
    country: "DE",
  },
  recipient: {
    name: {
      firstName: "Bob",
      lastName: "Andersson",
      entityType: "Person",
    },
    addressField: "Reimersholmsgatan 104",
    postalCode: "11740",
    city: "Stockholm",
    country: "SE",
  },
  targetAccount: {
    accountIdentifier: {
      ibanNumber: "SE5675512358273223854822",
      identifierType: "IBAN",
    },
    country: "SE",
  },
  purposeOfTransaction: "GIFT",
  reference: "b6c40f51-94d0-4bd3-bd92-a4da185b3a5c",
};

// The payload for the prepare payment request
const paymentInstruction: PaymentInstruction = {
  payinMemberName: 'ChangeMe',
  payinCurrency: 'ChangeMe',
  payoutMemberName: 'payout-fake',
  payoutCurrency: 'SEK',
  payoutService: 'INTEGRATION',
  fixedAmountSide: 'PAYOUT',
  fixedAmount: '0.5',
  amlInformation: amlInformation,
};

// Use ts-node paymentExample.ts inside this directory to run the example
paymentExample(accessTokenData, privateJwk, paymentInstruction, baseUrl);

async function paymentExample(accessTokenData: AccessTokenData, privateJWK: JWK, paymentInstruction: PaymentInstruction, baseUrl: string) {
  const accessTokenAsJWT = await CognitoBearerProvider.create(accessTokenData).then(provider => provider.getAccessTokenAsJWT());
  /** Prepare payment */

  // Make a request to the prepare payment endpoint to prepare a payment
  const preparePaymentResponse = await preparePayment(paymentInstruction, accessTokenAsJWT, baseUrl);

  /** Verifying the response from prepare payment request */

  const payoutMemberName = paymentInstruction.payoutMemberName;

  // Fetch the payout's public JWK
  const payoutJWK = await getPayoutJWK(payoutMemberName, accessTokenAsJWT, baseUrl);

  // Verify that the payment statement has been signed by the payout's JWK
  const verifiedPaymentStatement = await verifyJWS(preparePaymentResponse.signedPaymentStatement, payoutJWK);
  
  console.log(`Successfully verified signature for the payment statement, 
    valid until: ${verifiedPaymentStatement.validUntil} 
    payin member name: ${verifiedPaymentStatement.payin.memberName} 
    payout member name: ${verifiedPaymentStatement.payout.memberName} 
    payout currency: ${verifiedPaymentStatement.payout.currency} 
    payout fee: ${verifiedPaymentStatement.payout.fee} 
    payout maximum amount: ${verifiedPaymentStatement.payout.amountMax}
  `);

  /** Create payment */

  // Sign the payment statement hash
  const signedPaymentStatementHash = await createSignedPayload(preparePaymentResponse.paymentStatementHash, privateJWK);

  const paymentOrder: PaymentOrder = {
    signedPaymentStatementHash: signedPaymentStatementHash,
    paymentStatementHash: preparePaymentResponse.paymentStatementHash,
    payinCurrency: paymentInstruction.payinCurrency,
    fixedAmountSide: paymentInstruction.fixedAmountSide,
    fixedAmount: paymentInstruction.fixedAmount,
    triggerType: 'IMMEDIATE',
    paymentRef: uuidv4(),
  };

  /*********************************************************************************************
   * WorstAcceptablePrice is a safeguard amount to ensure a payment                            *
   * only gets accepted if the actual payin amount <= worstAcceptablePrice                     *
   * In short, it's how much you are willing to pay at most.                                   *
   *                                                                                           *
   * The indicative amount we got from the prepare payment response                            *
   * represents what we are expected to pay.                                                   *
   * The final payment amount fluctuate based on the exchange rate, which means that the final *
   * payment amount won't necessarily be the same as the indicative amount.                    *
   *********************************************************************************************/

  // As an example we will set the worstAcceptable price to 1% more than the indicative amount
  const worstAcceptablePrice = new BigDecimal(preparePaymentResponse.payinAmount)
    .multiply(new BigDecimal(1.01))
    .getValue();

  // Make a request to the create payment endpoint to create a payment
  const paymentResponse = await createPayment(
    paymentOrder,
    worstAcceptablePrice,
    accessTokenAsJWT,
    baseUrl,
  );

  /** Verifying the response from payment request */

  // We expect the payment to go through without any problems
  // If this is the case the state of the payment should be ACCEPTED
  if (paymentResponse.payment.state == 'ACCEPTED') {
    console.log('Payment was received by payout with the payoutId ' + paymentResponse.payment.payoutId);
  }

}

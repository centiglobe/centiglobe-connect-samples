# Centiglobe integration examples
[Centiglobe](https://centiglobe.com) provides example code in different languages to help bootstrap the integration towards the Centiglobe platform.

## Preparations
The members need to get credentials and be set up in the staging environment by Centiglobe before commencing any tests.
